"""
Command-line tool to manage starting and stopping of PondBot.
"""

import os
import sys

from pond.core import PondBot


if __name__ == "__main__":
    try:
        command = sys.argv[1]
    except IndexError:
        print("You did not specify a command.")
    else:
        if command == "start_bot":
            PondBot.start()
        elif command == "start":
            os.system("(python3 pond_manage.py start_bot &)")
        elif command == "stop":
            os.system("kill $(ps aux | grep '[p]ython3 pond_manage.py start_bot' | awk '{print $2}')")
        elif command == "restart":
            os.system("kill $(ps aux | grep '[p]ython3 pond_manage.py start_bot' | awk '{print $2}')")
            os.system("(python3 pond_manage.py start_bot &)")
        elif command == "--help":
            print("Help for pond_manage.py.\n"
                  "start_bot -- Start PondBot.\n"
                  "start ------ Start PondBot as a background job.\n"
                  "stop ------- Immediately kill any running instances of PondBot. (Only use this if the instances cannot be shut down gracefully)\n"
                  "restart ---- Immediately kill any running instances of PondBot and start a new instance in the background. (see warning in stop)\n")
        else:
            print("That is not a valid command.")
