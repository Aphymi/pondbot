"""
Core functions of PondBot, i.e. definition of the singleton PondBot class and
  various functions it uses.
Loading of this module triggers basic configuration of logging.
"""

import importlib
import logging
import os
import re
import time
import yaml

from irc.bot import SingleServerIRCBot

from pond import commands, cooldown

#Get the absolute path of the bot's configuration file.
CONFIG_PATH = os.path.join(os.path.dirname(__file__), "config.yml")

#Configure the bot's logging.
logging.basicConfig(
    filename=os.path.join(os.path.dirname(__file__), "log.txt"),
    format="[%(asctime)s] %(message)s",
    datefmt="%d/%m/%Y %H:%M:%S",
    level="INFO",
)

class PondBot(SingleServerIRCBot):
    """
    Master class to interface with the IRC server.
    
    PondBot is a singleton subclass of irc.bot.SingleServerIRCBot which manages most interfacing
      with the IRC server.
    
    Define several event handlers as well as a message parser to process commands and respond to
      regex triggers.
    """
    
    config = {}
    the_bot = None
    
    def __init__(self, server, channel, nick, port=6667):
        """
        Constructor for an instance of PondBot.
        
        Arguments:
        server --- A string containing IRC server to connect to. (e.g. "irc.esper.net")
        channel -- The channel to connect to on the IRC server. (e.g. "#lobby")
        nick ----- The nick that the bot should have upon connecting to the server. (e.g. "PondBot")
        port ----- The port at which the server accepts IRC connections. (default 6667)
        """
        
        super().__init__([(server, port)], nick, nick)
        self.channel = channel
    
    @classmethod
    def get_printer(cls, event):
        """
        Static method. Return a function that will print to the output corresponding to
          the event parameter.
        
        event.type should be one of ("pubmsg", "privmsg", "privnotice").
        Otherwise return a function that prints to standard output.
        
        Arguments:
        event -- Event that the print function will respond to.
        """
        
        connection = cls.the_bot.connection
        if event.type == "pubmsg":
            return lambda msg: connection.privmsg(event.target, format_string("&b" + str(msg)))
        elif event.type == "privmsg":
            return lambda msg: connection.privmsg(event.source, format_string("&b" + str(msg)))
        elif event.type == "privnotice":
            return lambda msg: connection.notice(event.source, format_string("&b" + str(msg)))
        else:
            logging.error("PondBot.get_printer called with illegal event type. %s" % event.__dict__)
            return print
    
    @classmethod
    def print(cls, event, msg):
        """
        Static convenience method. Print msg to the output corresponding to event.
        
        Pass event to get_printer, and pass msg to the returned print function.
        
        Arguments:
        event -- Event to respond to.
        msg ---- Message to print.
        """
        
        cls.get_printer(event)(msg)
    
    def on_nicknameinuse(self, connection, event):
        """
        Called when the nickname defined in the config is already in use. Append an
          underscore and try again.
        """
        
        connection.nick(connection.get_nickname() + "_")
    
    def on_welcome(self, connection, event):
        """
        Called when the IRC server welcomes the bot as a user.
        
        1. Send auth credentials defined in the config to the server.
        2. If the bot has needed to append an underscore in order to have a non-taken nick,
          attempt to ghost the user that currently owns the nick (likely a dead version of PondBot),
          wait two seconds (exact time is arbitrary. It must be long enough to give the server time
          to kick the original nick owner), then reclaim the nick.
        3. Join the channel specified in the config.
        """
        
        self.connection.privmsg("NickServ", "identify %s" % self.config["connection"]["password"])
        if connection.get_nickname() == self.config["connection"]["nick"] + "_":
            connection.privmsg("NickServ", "ghost %s" % self.config["connection"]["nick"])
            #If the Bot tries to /nick immediately, the server will not have kicked the first Bot yet.
            time.sleep(2)
            connection.nick(self.config["connection"]["nick"])
        connection.join(self.channel)
    
    def on_disconnect(self, connection, event):
        """
        Called when the bot is disconnected from the IRC server.
        """
        
        #By default, the bot will reconnect a while after being disconnected. If the bot was
        #  disconnected because of being ghosted by another version of the bot, it would ghost
        #  that bot when attempting to reconnect. This  would cause an infinite loop of the two
        #  bots disconnecting and reconnecting.
        #To prevent this, force the bot to disconnect from the server permanently.
        
        self.die("Bot was disconnected from the server.")
    
    def on_pubmsg(self, connection, event):
        """
        Called when a message is sent to the channel. Pass the event to parse_message().
        """
        
        self.parse_msg(connection, event)
    
    def on_privmsg(self, connection, event):
        """
        Called when a private message is sent to the bot. Pass the event to parse_message().
        """
        
        self.parse_msg(connection, event)
    
    def on_privnotice(self, connection, event):
        """
        Called when a notice is sent to the bot. Pass the event to parse_message().
        """
        
        #Ignore any messages from the server
        #Modify this line if connection.server in the config is changed.
        if event.source.endswith(".esper.net"):
            return
        self.parse_msg(connection, event)
    
    BRIDGE_BOT_CHAT = re.compile(r"^<\[(?P<rank>\w+?)\]-?(?P<name>[^ ]*)> (?P<message>.*)")
    BRIDGE_BOT_SERVER = re.compile(r"^\[Server\] (?P<message>.*)")
    BRIDGE_BOT_FLYING = re.compile(r"^\[-?(?P<name>[^ ]*) was KICKED \(Flying is not enabled on this server\)\]$")
    
    def parse_msg(self, connection, event):
        """
        Parse the message. Detect if the msg contains a command, or if it matches any of the
          regexes in the config.
        """
        
        try:
            #event.arguments[0] is the full message sent by the source.
            msg = remove_formatting(event.arguments[0])
            #Replace event.source with only the source's nick
            event.source = event.source[:event.source.index("!")]
            
            #If the message came from the MC bridge bot.
            if event.source.lower() == self.config["minecraft_bridge_nick"]:
                event.is_bot = True
                match = self.BRIDGE_BOT_CHAT.match(msg)
                if match:
                    event.source = match.group("name")
                    event.rank = match.group("rank")
                    msg = match.group("message")
                    event.arguments = [match.group("message")]
                    if (len(event.source) > 20 and cooldown.has_cooled_down("long_name_warning") and
                            self.config["ranks"].index(event.rank) <= self.config["ranks"].index("Mod")):
                        cooldown.set_cooldown("long_name_warning", 15)
                        self.print(event, "%s: Your name is %s characters. Please shorten it to "
                                            "twenty characters or fewer." %
                                            (event.source, len(event.source)))
                    if event.rank == "Owner":
                        event.rank = "Admin"
                
                else:
                    match = self.BRIDGE_BOT_SERVER.match(msg)
                    #If the message was made with /say <message>
                    if match:
                        event.source = "Server"
                        event.rank = "Admin"
                        msg = match.group("message")
                        event.arguments = [match.group("message")]
                    else:
                        match = self.BRIDGE_BOT_FLYING.match(msg)
                        if match and cooldown.has_cooled_down("kicked_for_flying"):
                            #If the message was someone getting kicked for flying, alert staff.
                            #Set a cooldown, because otherwise the message can get spammy.
                            cooldown.set_cooldown("kicked_for_flying",
                                                    self.config["kicked_for_flying_cooldown"])
                            self.print(event, "%s was kicked for flying!" % match.group("name"))
                        return
            else:
                #Determine the user's rank.
                event.rank = "Mod" if self.channels[self.channel].is_oper(event.source) else "Member"
                #Check if the user is defined in the config as some rank higher than Mod.
                if event.rank == "Mod" and event.source in (self.config.get("irc-ranks") or {}):
                    event.rank = self.config["irc-ranks"][event.source]
            
            #If the message is a command.
            if msg.startswith("!"):
                commands.handler.delegate_command(event)
            else:
                #If the source is a Member or lower, check for regexes.
                if self.config["ranks"].index(event.rank) <= self.config["ranks"].index("Member"):
                    regex_response = get_regex_response(msg)
                    if regex_response:
                        self.print(event, event.source + ": " + regex_response)
        
        except Exception:
            logging.exception("Error caught in PondBot.parse_msg.")
    
    @classmethod
    def start(cls):
        """
        Do everything necessary to start the bot and connect to the IRC server.
        """
        
        #Load every module in the commands package to ensure they are registered.
        for file in os.listdir(os.path.join(os.path.dirname(__file__), "commands")):
            name, ext = os.path.splitext(file)
            if ext == ".py" and name != "__init__":
                importlib.import_module("pond.commands." + name)
        
        #Load the config file.
        with open(CONFIG_PATH) as file:
            config = yaml.load(file)
        cls.config = config
        
        #Construct the bot instance.
        cls.the_bot = cls(
                    config["connection"]["server"],
                    config["connection"]["channel"],
                    config["connection"]["nick"],
        )
        
        commands.handler.load_commands_config()
        commands.quotes.load_quote_list()
        #Load all the regexes from the config.
        reload_regexes()
        
        try:
            super(PondBot, cls.the_bot).start()
        except:
            logging.exception()

format_pattern = re.compile(r"&([0-9a-flonr])")
def format_string(msg):
    """
    Format msg using IRC/Minecraft notation for formatting symbols and return it.
    """
    
    formats = {
        "0": "\00301", "1": "\00302", "2": "\00303",
        "3": "\00310", "4": "\00304", "5": "\00306",
        "6": "\00307", "7": "\00315", "8": "\00314",
        "9": "\00312", "a": "\00309", "b": "\00311",
        "c": "\00304", "d": "\00313", "e": "\00308",
        "f": "\00300", "l": "\002", "o": "\035",
        "n": "\037", "r": "\017\00311",
    }
    return format_pattern.sub(lambda match: formats[match.group(1)], msg)

def remove_formatting(msg):
    """
    Remove IRC/Minecraft formatting from msg and return it.
    """
    
    #Remove colours. Go through codes 15-0 inclusive backwards.
    for i in range(15, -1, -1):
        msg = msg.replace("\00300", "")
        msg = msg.replace("\003" + str(i), "")
        #Make sure to get 01, 02, etc. if they exist.
        msg = msg.replace("\003" + str(i).zfill(2), "")
    #Remove bold, underline, italics and reset characters
    for s in ("\002", "\035", "\037", "\017"):
        msg = msg.replace(s, "")
    return msg

def reload_regexes():
    """
    Compile and save the regexes defined in the config.
    
    Defined in a separate function in order to allow the regexes to be recompiled when the config
      is changed and reloaded.
    """
    
    PondBot.regexes = {re.compile(r"\b"+pattern+r"\b", re.IGNORECASE): response
                        for pattern, response in PondBot.config["regexes"].items()}

def get_regex_response(msg):
    """
    Check if msg matches any of the regexes defined in the config. If so, return the message
      associated with that regex.
    """
    
    for pattern in PondBot.regexes:
        match = pattern.search(msg)
        if match and cooldown.has_cooled_down("regex_" + pattern.pattern) and cooldown.has_cooled_down("regex"):
            cooldown.set_cooldown("regex_" + pattern.pattern, int(PondBot.config["regex_cooldown"]))
            return PondBot.regexes[pattern]
