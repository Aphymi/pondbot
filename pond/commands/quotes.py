"""
Module for commands involving quotes.

Keys of a quote object:
id ------- Unique identification number for the object. Note that IDs are generated linearly.
content -- The literal quotation.
date ----- The date that the quote was added to the saved list.
display -- Boolean to indicate whether a quote should be publicly displayed. Set to False when a
            quote is "deleted".

Commands:
  quote <id>
  addquote <quote>
  delquote/rmquote <id>
"""

from datetime import datetime
import json
import os
import random

from pond.commands.handler import Command

quote_list = []

QUOTE_FILE_PATH = os.path.join(os.path.dirname(__file__), "quotes.json")

def load_quote_list():
    """
    Load the quotes from QUOTE_FILE_PATH and save them to quote_list.
    """
    
    #Create the file if it doesn't exist.
    if not os.path.isfile(QUOTE_FILE_PATH):
        write_quotes_to_file()
    with open(QUOTE_FILE_PATH, "r") as file:
        quote_list.clear()
        quote_list.extend(json.load(file))

def write_quotes_to_file():
    """
    Write saved quotes to QUOTE_FILE_PATH.
    """
    
    with open(QUOTE_FILE_PATH, "w+") as file:
        json.dump(quote_list, file, indent="\t")

@Command(min_rank="Member", cooldown=15,
        args_validation="len(args) < 2 and (args[0].isdigit() or args[0] == 'r' or args[0] == 'random')",
        args_error="The quote number is the only parameter.")
def quote(print, num="random"):
    """
    Print a quote from the saved list.
    
    Arguments:
    num -- A number, "r", or "random". If a number, the ID of a quote; if "r" or "random",
            a random quote will be retrieved.
    """
    
    if num.lower() == "r" or num.lower() == "random":
        displayed_quotes = [quote for quote in quote_list if quote["display"]]
        if not displayed_quotes:
            print("The quote list is currently empty.")
            return
        quote = random.choice(displayed_quotes)
    else:
        num = int(num)
        if not 0 <= num < len(quote_list) or not quote_list[num]["display"]:
            print("That quote isn't on the list!")
            return
        quote = quote_list[num]
    print(quote["content"])

@Command(min_rank="SrOp", cooldown=.5)
def addquote(print, *text):
    """
    Add a quote to the saved list.
    
    Arguments:
    text --- The quote to be saved as a list of strings, split at spaces.
    """
    
    quote = {"id": len(quote_list),
            "content": " ".join(text),
            "date": str(datetime.today()),
            "display": True}
    quote_list.append(quote)
    write_quotes_to_file()
    print("Quote %s saved." % quote["id"])

@Command(min_rank="SrOp", cooldown=1,
        args_validation="len(args) == 1 and args[0].isdigit()",
        args_error="The quote number is the only parameter.")
def delquote(print, num):
    """
    Delete a quote from the saved list.
    
    Arguments:
    num -- The ID of the quote to be removed.
    """
    
    num = int(num)
    if not 0 <= num < len(quote_list):
        print("That quote isn't on the list!")
        return
    quote_list[num]["display"] = False
    write_quotes_to_file()
    print("Quote %s deleted." % num)

@Command(min_rank="SrOp", cooldown=1,
        args_validation="len(args) == 1 and args[0].isdigit()",
        args_error="The quote number is the only parameter.")
def restorequote(print, num):
    """
    Restore a quote that was deleted from the saved list.
    
    Arguments:
    num -- The ID of the quote to be restored.
    """
    num = int(num)
    if not 0 <= num < len(quote_list) or quote_list[num]["display"]:
        print("There is no deleted quote with that ID.")
        return
    quote_list[num]["display"] = True
    write_quotes_to_file()
    print("Quote %s restored." % num)
