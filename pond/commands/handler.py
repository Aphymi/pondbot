"""
Handle all command-related interaction with PondBot.

This is the only module in the commands package that does not itself contain commands.

Terminology:
static command --- A command that takes no arguments and modifies no data. A single command will
                        always output the same response, no matter the context.
dynamic command -- A command that may or may not take arguments and may or may not modify data.
                        It should always give output, and if the command does not modify data,
                        the output should vary depending on context.

Any command that is not a static command is a dynamic command, and vice versa.
"""

import functools
import logging

from pond import cooldown
from pond.core import PondBot

#Union of static_commands and dynamic_commands.
#Keys are the strings that the commands may be called by, and values are callable command objects.
commands = {}
#A dict of all static commands defined in PondBot's config.
static_commands = {}
#All commands that are not static commands. These may or may not take arguments and may or may not
#  modify data in some way. They should always return some kind of response.
dynamic_commands = {}

def load_commands_config():
    """
    Load and process the command-related information in PondBot's config.
    
    Such information includes static commands and command aliases.
    """
    
    #Empty and refill the static_commands dict.
    #This function is called when PondBot's configuration is reloaded, so static_commands may be nonempty.
    static_commands.clear()
    for name, msg in PondBot.config["static-commands"].items():
        #msg needs to be bound to the proper lambda. If the lambdas attempt to rely on closure,
        #  the lambda for every static command will refer to the same str object for message,
        #  resulting in every static command giving the same message (the one belonging to the
        #  msg that happened to belong to the last iteration of the loop.
        static_commands[name] = Command(min_rank="Member", static=True, cooldown=45)(
                                        lambda print, bound_msg=msg: print(bound_msg))
        #Commands are stored by __name__ in the command dict.
        static_commands[name].__name__ = name
    
    #Combine static_commands and dynamic_commands into commands.
    commands.clear()
    commands.update(dict(static_commands, **dynamic_commands))
    
    #Add aliases.
    for command in PondBot.config["aliases"]:
        for alias in PondBot.config["aliases"][command]:
            commands[alias] = commands[command]

#Ooooo! Delegate! That sounds like a very professional word! :D
def delegate_command(event):
    """
    From the event that triggered the command, find the corresponding Command object,
      validate that the command is usable, and call the command.
    """
    
    #Limit each argument to at most twenty characters. Don't want to process ridiculous input.
    msg = [arg[:20] for arg in event.arguments[0][1:].split(" ")]
    #Catches "!", "! ", "! foo", etc.
    if msg[0] == "":
        return
    cmd, event.arguments = msg[0].lower(), msg[1:]
    if cmd not in commands:
        return
    command = commands[cmd]
    if not validate_command(command, event):
        return
    logging.info("%s used the '%s' command with arguments %s." % (event.source, msg[0], msg[1:]))
    command(event, event.arguments)
    cooldown.set_cooldown(command.__name__, command.meta["cooldown"])

def validate_command(command, event):
    """
    Verify that the command is usable and has valid arguments.
    """
    
    #Prevent people using a command before it's cooled down.
    if not cooldown.has_cooled_down(command.__name__):
        pass
    #Prevent someone without the proper permission from using a command
    elif PondBot.config["ranks"].index(event.rank) < PondBot.config["ranks"].index(command.meta["min_rank"]):
        #If someone without perms tries to use !addquote, tell them how to suggest them
        if command == "addquote":
            event = object()
            event.type = "pubmsg"
            event.target = ""
            PondBot.print(event,
                    "To suggest a quote to be added to the bot, go to http://bit.ly/qcquotesqc")
        pass
    #Prevent someone from using a command with improper parameters
    elif not eval(command.meta["args_validation"], {"args": event.arguments}):
        PondBot.print(event, "Error: " + command.meta["args_error"])
    else:
        return True
    return False

class Command:
    """
    Decorator class to define a command for PondBot.
    
    All command objects will take a function as its first argument. That function should print to
      the appropriate output.
    
    Attributes:
      min_rank --------- Minimum rank a player must have in order to use the command.
      dscr ------------- Short description of the command.
      cooldown --------- Minimum time in seconds after a command is used that the same command can
                          be used again.
      args_validation -- Short expression to be eval()uated to detect if args are basically
                          correctly formatted. Will have access to args var.
      args_error ------- Message to be returned if argsValidation returns false.
      pass_event ------- Boolean for whether to pass the event that triggered the command. Includes
                          source, rank, etc.
    """
    
    def __init__(self, min_rank="Admin", cooldown=5, args_validation="True", args_error="",
                static=False, pass_event=False):
        self.meta = {
            "min_rank": min_rank,
            "cooldown": cooldown,
            "args_validation": args_validation,
            "args_error": args_error,
        }
        self.static = static
        self.pass_event = pass_event
    
    def __call__(self, cmd):
        @functools.wraps(cmd)
        def wrapped_func(event, args=[]):
            if self.static:
                #Don't pass any arguments that a static command might receive.
                args = []
            if self.pass_event:
                event.the_bot = PondBot.the_bot
                args.insert(0, event)
            cmd(PondBot.get_printer(event), *args)
        wrapped_func.meta = self.meta
        if not self.static:
            dynamic_commands[cmd.__name__] = wrapped_func
        return wrapped_func
