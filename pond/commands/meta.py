"""
Module for commands that relate to or modify PondBot itself.

Commands:
  disable <command|"commands"|"regex"|"*">...
  enable <command|"commands"|"regex"|"*">...
  shutdown
  reload
  nick <nickname>
  join <channel>
"""

import inspect
import yaml

from pond import cooldown
import pond.commands
from pond.commands.handler import Command, commands


@Command(min_rank="Mod", cooldown=2)
def disable(print, *features):
    """
    Disable all or specific features of PondBot.
    
    Arguments:
    features -- A list of features to disable. If the list is omitted or contains "*", disable all features
            of PondBot; if the list contains "commands", disable all commands; if the list contains
            "regex", disable PondBot responses for regex triggers.
    """
    
    command_dict = pond.commands.handler.commands  # @UndefinedVariable
    all_commands = set(command_dict.keys())
    features = set(features)
    if not features or "*" in features:
        features |= all_commands | {"regex"}
    if "commands" in features:
        features |= all_commands
    if "regex" in features:
        cooldown.set_cooldown("regex", forever=True)
    
    #for command in the intersection of features and commands, i.e. all features that are commands.
    for command in features.intersection(all_commands):
        cooldown.set_cooldown(command_dict[command].__name__, forever=True)
    #In case someone tried to disable the enable or disable commands.
    cooldown.set_cooldown("enable", -1)
    cooldown.set_cooldown("disable", -1)
    print("Disabled.")

@Command(min_rank="Mod", cooldown=2)
def enable(print, *features):
    """
    Re-enable all or specific features of PondBot.
    
    Arguments:
    features -- A list of features to enable. If the list is omitted or contains "*", enable all features
            of PondBot; if the list contains "commands", enable all commands; if the list contains
            "regex", enable PondBot responses for regex triggers.
    """
    
    command_dict = pond.commands.handler.commands  # @UndefinedVariable
    all_commands = set(command_dict.keys())
    features = set(features)
    if not features or "*" in features:
        features |= all_commands | {"regex"}
    if "commands" in features:
        features |= all_commands
    if "regex" in features:
        cooldown.set_cooldown("regex", -1)
    
    for command in features.intersection(all_commands):
        cooldown.set_cooldown(command_dict[command].__name__, -1)
    
    print("Re-enabled.")

@Command(min_rank="Admin", pass_event=True)
def shutdown(print, event):
    """
    Completely shutdown the bot.
    """
    
    print("Bye! o/")
    #Disconnect from the server gracefully.
    event.the_bot.disconnect()

@Command(min_rank="Op", cooldown=5)
def reload(print):
    """
    Reload PondBot's configuration file.
    """
    
    with open(pond.core.CONFIG_PATH) as file:
        pond.core.PondBot.config = yaml.load(file)
    pond.commands.handler.load_commands_config()
    pond.core.reload_regexes()
    print("Bot reloaded.")

@Command(min_rank="Admin", cooldown=10, pass_event=True,
        args_validation="len(args) == 1", args_error="You need to provide a nick!")
def nick(print, event, nickname):
    """
    Change PondBot's current nick.
    
    Arguments:
    nickname -- PondBot's new nick.
    """
    
    try:
        event.the_bot.connection.nick(nickname)
    except Exception as e:
        #I don't actually remember what this possible exception could be.
        print("Error: " + str(e))

@Command(min_rank="Member", cooldown=5,
        pass_event=True)
def join(print, event):
    """
    Make PondBot join the specified channel.
    """
    
    event.the_bot.connection.join(pond.core.PondBot.config["connection"]["channel"])
    print("Joined channel.")

@Command(min_rank="Member", cooldown=5, pass_event=True, args_validation="len(args) == 1",
        args_error="You need to provide a single command as only argument.")
def help(print, event, command):
    """
    Display help about a given command.
    
    Arguments:
    command -- Name of the command to get help for.
    """
    
    command = commands.get(command)
    if not command:
        print("Command not found.")
        return
    
    try:
        #Split the docstring because IRC protocol does not allow multi-line messages to be sent in one go.
        docstring = inspect.getdoc(command).replace("PondBot",
                            event.the_bot.config["connection"]["nick"]).split("\n")
        docstring.append("")
    
    except AttributeError:
        #Static commands don't have proper docstrings.
        docstring = []
        
    for line in docstring:
        print(line)
    
    print("Cooldown: %s seconds" % command.meta["cooldown"])
    print("Minimum rank to use: %s" % command.meta["min_rank"])
