"""
Functionality relating to PondBot commands.

All modules in this package will be loaded while PondBot is starting, and any commands in those modules will
  be registered with the command handler.
"""
