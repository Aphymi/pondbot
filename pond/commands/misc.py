"""
Module for miscellaneous commands that aren't part of a larger feature of the Bot.

Commands:
  welcome <username>
  roll <sides>
  mcstatus
  echo <message>
  kick <nick> [msg]
  kickban <nick> [msg]
  fuck <type> [args]
  temperature <degrees> <units>
"""

import random
import requests

from pond.commands.handler import Command
from pond.core import PondBot


@Command(min_rank="Mod", cooldown=3, args_validation="len(args) != 0",
        args_error="You need to give a player name!")
def welcome(print, *name):
    """
    Print an invitation to currently online players to welcome a new player.
    
    Arguments:
    name -- The name of the new player to welcome.
    """
    
    #On the off chance that the source puts spaces in the name.
    name = " ".join(name)
    print("A new Member! Everyone please welcome %s to the server! :D" % name)

@Command(min_rank="Member", cooldown=3,
        args_validation="len(args) == 1 and args[0].isdigit() and int(args[0]) > 0",
        args_error="You must give a single number greater than 0 as an argument.")
def roll(print, num):
    """
    Print a random number. 0 < printed number <= num
    
    Arguments:
    num -- The number of sides on the metaphorical die being rolled.
    """
    print(random.randint(1, int(num)))

@Command(min_rank="Member", cooldown=15)
def mcstatus(print):
    """
    Print the current status of Mojang's Minecraft servers.
    """
    
    #This returns data in a dumb format: [{"service1": "red"}, {"service2": "green"}]
    r = requests.get("http://status.mojang.com/check").json()
    down_services = [list(service)[0] for service in r if list(service.values())[0] != "green"]
    #This is the old login server. It's always down.
    if "minecraft.net" in down_services:
        down_services.pop("minecraft.net")
    if down_services:
        print("&4&lThe following services are down: " + ", ".join(down_services))
    else:
        print("&2&lAll services are up.")

@Command(min_rank="Op", cooldown=10, args_validation="len(args) > 0",
        args_error="You have to give something to say!")
def echo(print, *message):
    """
    Print the given arguments.
    
    Arguments:
    message -- Message to print.
    """
    
    #message is a list of strings, split at spaces
    print(" ".join(message))

@Command(min_rank="Mod", cooldown=2, args_validation="len(args) > 0",
        args_error="You must give a nick to kick.", pass_event=True)
def kick(print, event, nick, *msg):
    """
    Kick a user from the IRC channel.
    
    Arguments:
    nick --- The nick of the user to kick.
    msg ---- The message the user will receive upon being kicked from the channel.
            (default "You've been kicked from the channel.")
    """
    
    #msg is a list of strings, split at spaces
    msg = " ".join(msg)
    event.the_bot.connection.privmsg("ChanServ", "kick %s %s %s" %
                                    (PondBot.config["connection"]["channel"], nick,
                                    msg or "You've been kicked from the channel."))

@Command(min_rank="Mod", cooldown=2, args_validation="len(args) > 0",
        args_error="You must give a nick to ban.", pass_event=True)
def kickban(print, event, nick, *msg):
    """
    Kick and ban a user from the IRC channel.
    
    Arguments:
    nick --- The nick of the user to kick and ban from the channel.
    msg ---- The message the user will receive upon being kicked and banned from the channel.
            (default "You've been banned from the channel.")
    """
    
    #msg is a list of strings, split at spaces
    msg = " ".join(msg)
    event.the_bot.connection.privmsg("ChanServ", "ban %s %s!*@*" %
                                    (PondBot.config["connection"]["channel"], nick))
    event.the_bot.connection.privmsg("ChanServ", "kick %s %s %s" %
                                    (PondBot.config["connection"]["channel"], nick,
                                    msg or "You've been banned from the channel."))

@Command(min_rank="SrOp", cooldown=5, pass_event=True)
def fuck(print, event, *args):
    """
    Fuck you.
    """
    
    """
    Retrieve the message from Fuck Off As A Service corresponding to the given arguments.
    Note that the source of the command will always be given as the first argument to FOAAS.
    
    Also note that, unlike in FOAAS' documentation, the only argument that the bot will take for
      the "field" subcommand is the name of the user the command is being targeted at.
    
    Arguments:
    args --- Arguments to give FOAAS.
    """
    
    if args == ("help",):
        print(event.source + ": http://foaas.com/")
        return
    #Makes the field subcommand less unintuitive. !f field <target>
    #Sent to FOAAS as <source> <target> <source>
    if len(args) == 2 and args[0] == "field":
        args = ("field", event.source, args[1])
    r = requests.get("http://foaas.com/" + "/".join(args + (event.source,)),
                    headers={"accept": "text/plain"})
    #FOAAS couldn't parse the query, it either gave 622 (silly version of 404) or a full HTML page
    if r.status_code == 622 or len(r.text) > 500:
        print("%s invites you to consider the truly monumental amount of fucks it couldn't give about your request." %
                        PondBot.config["connection"]["nick"])
    else:
        print(r.text)

@Command(min_rank="Member", cooldown=5,
        args_validation="len(args) == 2 and args[1].lower() in ('c', 'f')",
        args_error="This command needs two arguments: A number, followed by C or F.")
def temperature(print, degrees, units):
    """
    Convert between Celsius and Fahrenheit.
    
    Arguments:
    temp --- Number of degrees.
    units -- Units of temperature, "C" or "F", that temp is given in.
    """
    
    try:
        degrees = float(degrees)
    except ValueError:
        print("This command needs two arguments: A number, followed by C or F.")
    if units.lower() == "c":
        print("%s in Celsius is %s in Fahrenheit." % (degrees, round(degrees*(9/5) + 32, 3)))
    else:
        print("%s in Fahrenheit is %s in Celsius." % (degrees, round((degrees-32) * (5/9), 3)))
